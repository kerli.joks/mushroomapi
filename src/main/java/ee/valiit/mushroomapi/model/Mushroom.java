package ee.valiit.mushroomapi.model;

public class Mushroom {
    private int id;
    private String nameEe;
    private String nameLd;
    private String family;
    private String picture;
    private String color;
    private String body;
    private String hat;
    private String meat;
    private String petals;
    private String habitat;
    private String harvest;
    private String preparation;
    private String preservation;

    public Mushroom() {

    }

    public Mushroom(int id, String nameEe, String nameLd, String family, String picture, String color, String body, String hat,
                    String meat, String petals, String habitat, String harvest, String preparation, String preservation) {
        this.id = id;
        this.nameEe = nameEe;
        this.nameLd = nameLd;
        this.family = family;
        this.picture = picture;
        this.color = color;
        this.body = body;
        this.hat = hat;
        this.meat = meat;
        this.petals = petals;
        this.habitat = habitat;
        this.harvest = harvest;
        this.preparation = preparation;
        this.preservation = preservation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameEe() {
        return nameEe;
    }

    public void setNameEe(String nameEe) {
        this.nameEe = nameEe;
    }

    public String getNameLd() {
        return nameLd;
    }

    public void setNameLd(String nameLd) {
        this.nameLd = nameLd;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getHat() {
        return hat;
    }

    public void setHat(String hat) {
        this.hat = hat;
    }

    public String getMeat() {
        return meat;
    }

    public void setMeat(String meat) {
        this.meat = meat;
    }

    public String getPetals() {
        return petals;
    }

    public void setPetals(String petals) {
        this.petals = petals;
    }

    public String getHabitat() {
        return habitat;
    }

    public void setHabitat(String habitat) {
        this.habitat = habitat;
    }

    public String getHarvest() {
        return harvest;
    }

    public void setHarvest(String harvest) {
        this.harvest = harvest;
    }

    public String getPreparation() {
        return preparation;
    }

    public void setPreparation(String preparation) {
        this.preparation = preparation;
    }

    public String getPreservation() {
        return preservation;
    }

    public void setPreservation(String preservation) {
        this.preservation = preservation;
    }
}

