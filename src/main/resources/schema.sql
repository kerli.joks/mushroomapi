DROP TABLE IF EXISTS `seentetabel`;
CREATE TABLE IF NOT EXISTS `seentetabel` (
`id` int(11) NOT NULL AUTO_INCREMENT,
  `nameEe` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nameLd` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `family` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hat` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meat` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `petals` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `habitat` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harvest` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `preparation` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `preservation` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index 2` (`nameEe`)
  );