package ee.valiit.mushroomapi.controller;

import ee.valiit.mushroomapi.model.Mushroom;
import ee.valiit.mushroomapi.repository.MushroomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@CrossOrigin("*")

public class MushroomController {
    @Autowired
    private MushroomRepository mushroomRepository;

    @GetMapping("/seened")
    public List<Mushroom> getMushroom() {
        return mushroomRepository.fetchMushroom();
    }

    @GetMapping("/seentetabel")
    public Mushroom getMushroom(@RequestParam("id") int id ) {
        return mushroomRepository.fetchMushroom(id);
    }

//    @DeleteMapping("/seentetabel")
//    public void deleteMushroom(@RequestParam("id") int id ) {
//        mushroomRepository.deleteMushroom(id);
//    }

    @PostMapping("/seentetabel")
    public void addMushroom(@RequestBody Mushroom m) {
        mushroomRepository.addMushroom(m);
    }
    @PutMapping("/seentetabel")
    public void updateMushroom(@RequestBody Mushroom mushroom) {
        mushroomRepository.updateMushroom(mushroom);
    }
}
